# pip install -r requirements.txt
# ultralytics==8.0.20
numpy==1.23.1

GDAL==3.0.4
# geolib
fiona==1.8
# pygeohash
# shapely

# Base Bytetrack ----------------------------------------
ipython  # interactive notebook
Pillow>=7.1.2
psutil  # system resources
PyYAML>=5.3.1
requests>=2.23.0
scipy>=1.4.1
# thop>=0.1.1  # FLOPs computation
tqdm>=4.64.0
pandas>=1.1.4
zmq
easydict

# ByteTrack -------------------------------------------------------------------
cython_bbox 
gdown
lap


# opencv_python